export const CheapFlightService = ['$http', ($http) => {
  const url = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights';

  return {
    find: (criteria) => {
      if (typeof criteria.start === 'undefined' || typeof criteria.end === 'undefined'
        || typeof criteria.origin === 'undefined' || typeof criteria.destination === 'undefined') {
        return Promise.resolve([]);
      }

      const finalUrl = `${url}/from/${criteria.origin}/to/${criteria.destination}/${criteria.start}/${criteria.end}/250/unique/?limit=15&offset-0`;

      return $http.get(finalUrl)
          .then(response => response.data.flights)
          .catch(() => Promise.resolve([]));
    }
  };
}];

export default CheapFlightService;

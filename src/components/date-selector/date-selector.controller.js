export default function DateSelectorController() {

  let ctrl = this;

  ctrl.dateSelected = () => {
    ctrl.onDateSelected({"selected":ctrl.date});
  }

}
